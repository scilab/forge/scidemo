// This help file was automatically generated using helpupdate
// PLEASE DO NOT EDIT
demopath = get_absolute_file_path("scidemo.dem.gateway.sce");
subdemolist = [
"scidemo_plotedolinear", "scidemo_plotedolinear.sce"; ..
"scidemo_plotcmplxfun", "scidemo_plotcmplxfun.sce"; ..
"scidemo_optimrosenbrock", "scidemo_optimrosenbrock.sce"; ..
"scidemo_normpdf", "scidemo_normpdf.sce"; ..
"scidemo_normaldist", "scidemo_normaldist.sce"; ..
"scidemo_matrixeigshow", "scidemo_matrixeigshow.sce"; ..
"scidemo_edolinear", "scidemo_edolinear.sce"; ..
"scidemo_cmplxfacets", "scidemo_cmplxfacets.sce"; ..
"rosenbrock_show", "rosenbrock_show.sce"; ..
"normdist_show", "normdist_show.sce"; ..
"eigenvalue_show", "eigenvalue_show.sce"; ..
"edolinear_show", "edolinear_show.sce"; ..
"complex_functions", "complex_functions.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
