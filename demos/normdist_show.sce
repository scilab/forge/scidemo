// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// TODO : make a dynamic update for improved interactivity

function demo_normdistshow ()
    //
    // Runs the main dialog:
    // * Plots the controls
    // * Runs the initial plot with default settings.

    hplot = scf();
    //
    // Compute widths and heights
    // The width of the graphics window
    gwidth = 200;
    // The height of the graphics window
    gheight = 200;
    // The width of the controls
    cwidth = round(0.9*gwidth);
    // The height of the controls
    cheight = 20;

    //
    // Now create the interactive dialog
    hmain=scf();
    drawlater()
    hfig=figure(hmain);
    hfig.axes_size = [gwidth gheight];
    //
    // A text to display current mu
    y = hfig.axes_size(2)-cheight;
    huimu=uicontrol(hfig,"style","text");
    huimu.Position = [10 y cwidth cheight];
    huimu.String = "mu=";
    huimu.BackgroundColor=[1 1 1];
    //
    // A slider to set mu
    y = y-cheight;
    huislidermu=uicontrol(hfig,"style","slider");
    huislidermu.Position = [10 y cwidth cheight];
    huislidermu.Min = 0;
    huislidermu.Max = 256;
    huislidermu.Value = 128;
    huislidermu.Callback = "demonodi_cbkupdtmu";
    //
    // A text to display current sigma
    y = y-cheight;
    huisigma=uicontrol(hfig,"style","text");
    huisigma.Position = [10 y cwidth cheight];
    huisigma.String = "sigma=";
    huisigma.BackgroundColor=[1 1 1];
    //
    // A slider to set sigma
    y = y-cheight;
    huislidersigma=uicontrol(hfig,"style","slider");
    huislidersigma.Position = [10 y cwidth cheight];
    huislidersigma.Min = 0;
    huislidersigma.Max = 256;
    huislidersigma.Value = 128;
    huislidersigma.Callback = "demonodi_cbkupdtsigma";
    //
    // A text to display current number of random numbers
    y = y-cheight;
    huinrand=uicontrol(hfig,"style","text");
    huinrand.Position = [10 y cwidth cheight];
    huinrand.String = "nrand=";
    huinrand.BackgroundColor=[1 1 1];
    //
    // A slider to set nrand
    y = y-cheight;
    huislidernrand=uicontrol(hfig,"style","slider");
    huislidernrand.Position = [10 y cwidth cheight];
    huislidernrand.Min = 0;
    huislidernrand.Max = 256;
    huislidernrand.Value = 128;
    huislidernrand.Callback = "demonodi_cbkupdtnrand";
    //
    // A text to display current number of classes in the histogram
    y = y-cheight;
    huinclass=uicontrol(hfig,"style","text");
    huinclass.Position = [10 y cwidth cheight];
    huinclass.String = "nclasses=";
    huinclass.BackgroundColor=[1 1 1];
    //
    // A slider to set nclass
    y = y-cheight;
    huislidernclass=uicontrol(hfig,"style","slider");
    huislidernclass.Position = [10 y cwidth cheight];
    huislidernclass.Min = 0;
    huislidernclass.Max = 256;
    huislidernclass.Value = 128;
    huislidernclass.Callback = "demonodi_cbkupdtnclass";
    //
    // A button to update the graphics
    y = y-cheight;
    huiupdate=uicontrol(hfig,"style","pushbutton");
    huiupdate.Position = [10 y cwidth cheight];
    huiupdate.String = "Update";
    huiupdate.BackgroundColor=[0.9 0.9 0.9];
    huiupdate.Callback = "demonodi_cbkupdtplot";
    
    global nordisshowgui;
    nordisshowgui = tlist([
        "NORDISTSHOGUI"
        "hplot"
        "hmain"
        "hfig"
        "huimu"
        "huislidermu"
        "huisigma"
        "huislidersigma"
        "huinrand"
        "huislidernrand"
        "huinclass"
        "huislidernclass"
        "boundsmu"
        "boundssigma"
        "boundsnrand"
        "boundsnclass"
        "mu"
        "sigma"
        "nrand"
        "nclass"
        "xmin"
        "xmax"
    ])
    nordisshowgui.hplot = hplot;
    nordisshowgui.hmain = hmain;
    nordisshowgui.hfig = hfig;
    nordisshowgui.huimu = huimu;
    nordisshowgui.huislidermu = huislidermu;
    nordisshowgui.huisigma = huisigma;
    nordisshowgui.huislidersigma = huislidersigma;
    nordisshowgui.huinrand = huinrand;
    nordisshowgui.huislidernrand = huislidernrand;
    nordisshowgui.huinclass = huinclass;
    nordisshowgui.huislidernclass = huislidernclass;
    nordisshowgui.mu = [];
    nordisshowgui.sigma = [];
    nordisshowgui.nrand = [];
    nordisshowgui.nclass = [];
    nordisshowgui.xmin = -20;
    nordisshowgui.xmax = 20;
    
    //
    // Setup bounds for the parameters
    nordisshowgui.boundsmu = [-20 20];
    nordisshowgui.boundssigma = [0.1 10];
    nordisshowgui.boundsnrand = [1 10000];
    nordisshowgui.boundsnclass = [1 200];
    //
    drawnow()
    demonodi_cbkupdtmu ()
    demonodi_cbkupdtsigma ()
    demonodi_cbkupdtnrand ()
    demonodi_cbkupdtnclass ()
    //
    // Update the plot
    demonodi_cbkupdtplot ()
endfunction

function v = demonodi_scaledata ( sliderhandle , valbounds )
    // Scales the data from the slider in sliderhandle into bounds in valbounds.
    t = sliderhandle.Value
    tMin = sliderhandle.Min
    tMax = sliderhandle.Max
    s = (t - tMin) / (tMax - tMin)
    v = s * valbounds(2) + (1-s) * valbounds(1)
endfunction
function demonodi_cbkupdtmu ()
    global nordisshowgui;
    nordisshowgui.mu = demonodi_scaledata ( nordisshowgui.huislidermu , nordisshowgui.boundsmu )
    nordisshowgui.huimu.String = msprintf("mu=%.3f",nordisshowgui.mu);
endfunction

function demonodi_cbkupdtsigma ()
    global nordisshowgui;
    //
    // We cannot authorize sigma=0
    if ( nordisshowgui.huislidersigma.Value == 0 ) then
      nordisshowgui.huislidersigma.Value = 1
    end
    nordisshowgui.sigma = demonodi_scaledata ( nordisshowgui.huislidersigma , nordisshowgui.boundssigma )
    nordisshowgui.huisigma.String = msprintf("sigma=%.3f",nordisshowgui.sigma);
endfunction

function demonodi_cbkupdtnrand ()
    global nordisshowgui;
    nordisshowgui.nrand = round(demonodi_scaledata ( nordisshowgui.huislidernrand , nordisshowgui.boundsnrand ))
    nordisshowgui.huinrand.String = msprintf("nrand=%d",nordisshowgui.nrand);
endfunction

function demonodi_cbkupdtnclass ()
    global nordisshowgui;
    nordisshowgui.nclass = round(demonodi_scaledata ( nordisshowgui.huislidernclass , nordisshowgui.boundsnclass ))
    nordisshowgui.huinclass.String = msprintf("nclass=%d",nordisshowgui.nclass);
endfunction

function demonodi_cbkupdtplot ()
    global nordisshowgui;
    scf(nordisshowgui.hplot)
    //
    drawlater()
    clf()
    scidemo_normaldist ( nordisshowgui.mu , nordisshowgui.sigma , nordisshowgui.nrand , nordisshowgui.nclass  , nordisshowgui.xmin , nordisshowgui.xmax );
    if ( %f ) then
    nordisshowgui.hplot.children.children(5).visible="on"; // The histogram
    nordisshowgui.hplot.children.children(6).visible="off"; // The PDF
    end
    //
    // Fix the ymin,ymax
    nordisshowgui.hplot.children.data_bounds = [
      nordisshowgui.xmin  -0.01
      nordisshowgui.xmax   0.2
    ];
    //
    drawnow()
endfunction


demo_normdistshow ();
clear demo_normdistshow;

