// Copyright (C) 2001 - Bruno Pincon
// Copyright (C) 2008 - INRIA
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// TODO : report the interactive controls from http://gitweb.scilab.org/?p=scilab.git;a=blob;f=scilab/modules/graphics/demos/cmplxfunc/demo_custom.dem.sce;h=7a35731a5321034e849d8d8456c14f18afbb26d2;hb=HEAD

function demo_cmplxfunc()
    subdemolist = [
    "acos"  "scidemo_plotcmplxfun(2,%eps,""Square"",""Ox"",41,""acos"",-110,75,[-1,1]);"
    "acosh" "scidemo_plotcmplxfun(2,%eps,""Square"",""Ox"",41,""acosh"",-110,75,[1,2]);"
    "asin"  "scidemo_plotcmplxfun(2,%eps,""Square"",""Ox"",41,""asin"",-110,75,[-1,1]);"
    "asinh" "scidemo_plotcmplxfun(2,%eps,""Square"",""Oy"",41,""asinh"",-110,75,[-2,2]);"
    "atanh" "scidemo_plotcmplxfun(2,0.001,""Square"",""Ox"",41,""atanh"",-110,75,[-0.99,0.99]);"
    "atan"  "scidemo_plotcmplxfun(2,0.001,""Square"",""Oy"",41,""atan"",-110,75,[-2,2]);"
    "cos"   "scidemo_plotcmplxfun(%pi,0,""Disk"",""Ox"",[40 20],""cos"",18,43,[%pi,%pi]);"
    "cosh"  "scidemo_plotcmplxfun(%pi,0,""Disk"",""Ox"",[40 20],""cosh"",-130,56,[-%pi,%pi]);"
    "exp"   "scidemo_plotcmplxfun(2,0,""Disk"",""Ox"",[40 20],""exp"",-130,73,[-2,2]);"
    "sin"   "scidemo_plotcmplxfun(%pi,0.001,""Disk"",""Ox"",[40 20],""sin"",-130,73,[-%pi,%pi]);"
    "sinh"  "scidemo_plotcmplxfun(%pi,0,""Disk"",""Ox"",[40 20],""sinh"",-148,60,[-%pi,%pi]);"
    "tan"   "scidemo_plotcmplxfun(%pi/2-0.15,0,""Square"",""Ox"",41,""tan"",-130,73,[-%pi/2+0.15,%pi/2-0.15]);"
    "tanh"  "scidemo_plotcmplxfun(%pi/2-0.2,0,""Square"",""Ox"",41,""tanh"",-130,73,[-%pi/2+0.2,%pi/2-0.2]);"
    "log"   "scidemo_plotcmplxfun(4,0.001,""Disk"",""Ox"",[40 20],""log"",30,60,[0.001,4]);"
    ];

    demochoice = strcat(subdemolist(:,1),"|");

    //
    // Compute widths and heights
    // The width of the graphics window
    gwidth = 200;
    // The height of the graphics window
    gheight = 250;
    
    //
    // Create the plot
    hcmplplot = scf();

    //
    // Create the control
    hmain=scf();
    hfig=figure(hmain);
    hfig.axes_size = [gwidth gheight];
    // create a figure
    y = hfig.axes_size(2)-240;
    huicont=uicontrol(hfig,"style","listbox");
    huicont.Position = [10 0 150 240];
    huicont.String = demochoice;
    huicont.Value = 1;
    huicont.Callback = "demo_cmplxfunccallback";
    huicont.BackgroundColor=[1 1 1];
    
    //
    // Create a data structure to share
    global complexfunctiongui;
    complexfunctiongui = tlist([
      "CMPLXFUNGUI" 
      "hmain" 
      "hfig" 
      "huicont" 
      "subdemolist"
      "hcmplplot"
      ])
    complexfunctiongui.hmain = hmain;
    complexfunctiongui.hfig = hfig;
    complexfunctiongui.huicont = huicont;
    complexfunctiongui.subdemolist = subdemolist;
    complexfunctiongui.hcmplplot = hcmplplot;
    
    //
    // Update the plot
    demo_cmplxfunccallback()
endfunction

function demo_cmplxfunccallback ()
    //
    // The user selected another function

    global complexfunctiongui;
    hmain = complexfunctiongui.hmain;
    hfig = complexfunctiongui.hfig;
    huicont = complexfunctiongui.huicont;
    subdemolist = complexfunctiongui.subdemolist;
    //
    // Get the choice of the user
    demochoice = hmain.children(1).Value
    //
    // Execute the statement
    drawlater()
    scf(complexfunctiongui.hcmplplot);
    clf(complexfunctiongui.hcmplplot);
    execstr(subdemolist(demochoice,2))
    drawnow()
endfunction


demo_cmplxfunc();
clear demo_cmplxfunc();


