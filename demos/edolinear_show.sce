// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function demo_edolinshow ()

    //
    // Create the plot
    hedoplot = scf();
    
    //
    // Compute widths and heights
    // The width of the graphics window
    gwidth = 250;
    // The height of the graphics window
    gheight = 500;
    // The width of the controls
    cwidth = round(0.9*gwidth);
    // The height of the controls
    cheight = 20;

    //
    // Create the controls
    hmain=scf();
    drawlater()
    //
    hfig=figure(hmain);
    hfig.axes_size = [gwidth gheight];
    //
    // A text for the solvers
    y = hfig.axes_size(2)-cheight;
    huimattext=uicontrol(hfig,"style","text");
    huimattext.Position = [10 y cwidth cheight];
    huimattext.String = "Matrices:";
    huimattext.BackgroundColor=[0.9 0.9 0.9];
    //
    // A listbox for the base matrix
    y = y-120;
    huilist=uicontrol(hfig,"style","listbox");
    huilist.Position = [10 y cwidth 120];
    huilist.String = [
    "[1 2;3 4]"
    "[1 2;-6 4]"
    "[1 0;0 2]"
    "[1 1;1 1]"
    "[0 0;0 0]"
    "[-1 0;0 1]"
    "[-1 0;0 -1]"
    ];
    huilist.Value = 1;
    huilist.BackgroundColor=[1 1 1];
    huilist.Callback = "demo_edolinshowcallback";
    //
    // A text for the symmetry
    y = y-cheight;
    huisymtext=uicontrol(hfig,"style","text");
    huisymtext.Position = [10 y cwidth cheight];
    huisymtext.String = "Symmetry:0";
    huisymtext.BackgroundColor=[0.9 0.9 0.9];
    //
    // A slider for the symmetry
    y = y-cheight;
    huisym=uicontrol(hfig,"style","slider");
    huisym.Position = [10 y cwidth cheight];
    huisym.Min = 0;
    huisym.Max = 256;
    huisym.Value = 0;
    huisym.Callback = "demo_edolinupdatesym";
    //
    // A text for the matrix
    y = y-cheight;
    huimattext=uicontrol(hfig,"style","text");
    huimattext.Position = [10 y cwidth cheight];
    huimattext.String = "Matrix=";
    huimattext.BackgroundColor=[1 1 1];
    //
    // A text for the eigenvalue #1
    y = y-cheight;
    huispectext1=uicontrol(hfig,"style","text");
    huispectext1.Position = [10 y cwidth cheight];
    huispectext1.String = "D1=";
    huispectext1.BackgroundColor=[1 1 1];
    //
    // A text for the eigenvalue #2
    y = y-cheight;
    huispectext2=uicontrol(hfig,"style","text");
    huispectext2.Position = [10 y cwidth cheight];
    huispectext2.String = "D2=";
    huispectext2.BackgroundColor=[1 1 1];
    //
    // A text for the solvers
    y = y-cheight;
    huisoltext=uicontrol(hfig,"style","text");
    huisoltext.Position = [10 y cwidth cheight];
    huisoltext.String = "Solvers:";
    huisoltext.BackgroundColor=[0.9 0.9 0.9];
    //
    // A listbox for the solvers
    y = y-80;
    huisollist=uicontrol(hfig,"style","listbox");
    huisollist.Position = [10 y cwidth 80];
    huisollist.String = ["adams" "stiff" "rk" "rkf" "fix"];
    huisollist.Value = 1;
    huisollist.BackgroundColor=[1 1 1];
    huisollist.Callback = "demo_edolinshowcallback";
    //
    // A text for rtol
    y = y-cheight;
    huirtoltext=uicontrol(hfig,"style","text");
    huirtoltext.Position = [10 y cwidth cheight];
    huirtoltext.String = "Relative Tolerance:";
    huirtoltext.BackgroundColor=[0.9 0.9 0.9];
    //
    // A slider for rtol
    y = y-cheight;
    huirtolsli=uicontrol(hfig,"style","slider");
    huirtolsli.Position = [10 y cwidth cheight];
    huirtolsli.Min = 0;
    huirtolsli.Max = 256;
    huirtolsli.Value = 128;
    huirtolsli.Callback = "demo_edolinupdatertol";
    //
    // A text for atol
    y = y-cheight;
    huiatoltext=uicontrol(hfig,"style","text");
    huiatoltext.Position = [10 y cwidth cheight];
    huiatoltext.String = "Relative Tolerance:";
    huiatoltext.BackgroundColor=[0.9 0.9 0.9];
    //
    // A slider for atol
    y = y-cheight;
    huiatolsli=uicontrol(hfig,"style","slider");
    huiatolsli.Position = [10 y cwidth cheight];
    huiatolsli.Min = 0;
    huiatolsli.Max = 256;
    huiatolsli.Value = 128;
    huiatolsli.Callback = "demo_edolinupdateatol";
    //
    // A button to update the graphics
    y = y-cheight;
    huiupdate=uicontrol(hfig,"style","pushbutton");
    huiupdate.Position = [10 y cwidth cheight];
    huiupdate.String = "Update";
    huiupdate.BackgroundColor=[0.9 0.9 0.9];
    huiupdate.Callback = "demo_edolinshowcallback";
    //
    drawnow()

    global edolinshowgui;
    edolinshowgui = tlist([
      "EIGSHOGUI" 
      "hedoplot" 
      "hmain"
      "hfig"
      "huilist"
      "huisym"
      "huisymtext"
      "huimattext"
      "huispectext1"
      "huispectext2"
      "huisollist"
      "huirtoltext"
      "huirtolsli"
      "huiatoltext"
      "huiatolsli"
      "huirtolbounds"
      "huiatolbounds"
      "rtol"
      "atol"
      "B"
    ])
    edolinshowgui.hedoplot = hedoplot;
    edolinshowgui.hmain = hmain;
    edolinshowgui.hfig = hfig;
    edolinshowgui.huilist = huilist;
    edolinshowgui.huisym = huisym;
    edolinshowgui.huisymtext = huisymtext;
    edolinshowgui.huimattext = huimattext;
    edolinshowgui.huispectext1 = huispectext1;
    edolinshowgui.huispectext2 = huispectext2;
    edolinshowgui.huisollist = huisollist;
    edolinshowgui.huirtolsli = huirtolsli;
    edolinshowgui.huiatolsli = huiatolsli;
    edolinshowgui.huirtoltext = huirtoltext;
    edolinshowgui.huiatoltext = huiatoltext;
    edolinshowgui.huirtolbounds = [-1 -10];
    edolinshowgui.huiatolbounds = [-1 -10];
    edolinshowgui.B = [];
    edolinshowgui.rtol = [];
    edolinshowgui.atol = [];
    //
    // Update the plot
    demo_edolinupdatertol ()
    demo_edolinupdateatol ()
    demo_edolinshowcallback ()
endfunction

function demo_edolinupdatesym ()
    //
    // The user updated the symetry

    global edolinshowgui;

    //
    instr = "A="+edolinshowgui.huilist.String(edolinshowgui.huilist.Value)
    execstr(instr)
    s = demoedli_scaledata ( edolinshowgui.huisym , [0 1] )
    B = s * (A+A')/2 + (1-s) * A
    edolinshowgui.B = B
    edolinshowgui.huisymtext.String = "Symmetry:"+string(s);
    edolinshowgui.huimattext.String = "Matrix="+msprintf("[%.3f %.3f;%.3f %.3f]",B(1,1),B(1,2),B(2,1),B(2,2));
    //
    // Compute the eigenvalues
    D = spec(B)
    if ( isreal(D(1)) ) then
      d1 = msprintf("%.3f",D(1))
    else
      d1 = msprintf("%.3f + i*%.3f",real(D(1)),imag(D(1)))
    end
    if ( isreal(D(2)) ) then
      d2 = msprintf("%.3f",D(2))
    else
      d2 = msprintf("%.3f + i*%.3f",real(D(2)),imag(D(2)))
    end
    edolinshowgui.huispectext1.String = "D1=" + d1;
    edolinshowgui.huispectext2.String = "D2=" + d2;

endfunction

function v = demoedli_scaledata ( sliderhandle , valbounds )
    // Scales the data from the slider in sliderhandle into bounds in valbounds.
    t = sliderhandle.Value
    tMin = sliderhandle.Min
    tMax = sliderhandle.Max
    s = (t - tMin) / (tMax - tMin)
    v = s * valbounds(2) + (1-s) * valbounds(1)
endfunction

function demo_edolinupdatertol ()
    //
    // The user updated rtol

    global edolinshowgui;

    rtol = 10^demoedli_scaledata ( edolinshowgui.huirtolsli , edolinshowgui.huirtolbounds )
    edolinshowgui.huirtoltext.String = msprintf("Relative Tolerance: %.3e",rtol)
    edolinshowgui.rtol = rtol
endfunction

function demo_edolinupdateatol ()
    //
    // The user updated atol

    global edolinshowgui;

    atol = 10^demoedli_scaledata ( edolinshowgui.huiatolsli , edolinshowgui.huiatolbounds )
    edolinshowgui.huiatoltext.String = msprintf("Absolute Tolerance: %.3e",atol)
    edolinshowgui.atol = atol
endfunction

function demo_edolinshowcallback ()
    //
    // The user wants to update the graphics
    
    //
    global edolinshowgui;
    
    // The number of time samples.
    m = 100;
    // The initial state.
    y0 = [1;10];
    // Initial and final times.
    t0 = 0;
    tf = 5;
    t = linspace(t0,tf,100);
    //
    drawlater()
    //
    clf(edolinshowgui.hedoplot);
    scf(edolinshowgui.hedoplot);
    //
    // Compute the matrix B by symetrization of A
    demo_edolinupdatesym()
    //
    // Update the plot
    //
    solname = edolinshowgui.huisollist.String(edolinshowgui.huisollist.Value)
    scidemo_plotedolinear ( y0 , t0 , t , edolinshowgui.B , edolinshowgui.rtol , edolinshowgui.atol , solname )
    //
    drawnow()

endfunction


demo_edolinshow();
clear demo_edolinshow;


