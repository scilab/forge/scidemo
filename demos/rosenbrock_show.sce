// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// TODO : add a text for x*

function demo_rosenbrockshow ()
    //
    // Runs the main dialog:
    // * Plots the contours of Rosenbrock
    // * Sets and plots default x0 and xopt

    hplot = scf();
    //
    // 1. Define rosenbrock for contouring
    function f = rosenbrockC ( x1 , x2 )
        x = [x1 x2];
        f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
    endfunction
    x0 = [-1.2 1.0];
    xopt = [1.0 1.0];
    //
    // 2. Draw the contour of Rosenbrock's function
    xdata = linspace(-2,2,100);
    ydata = linspace(-2,2,100);
    contour ( xdata , ydata , rosenbrockC , [1 10 100 500 1000])
    plot(x0(1) , x0(2) , "b.")
    xtitle("The Rosenbrock Test Case","x1","x2")


    //
    // Now create the interactive dialog
    hmain=scf();
    drawlater()
    hfig=figure(hmain);
    hfig.axes_size = [200 250];
    //
    // A text to display current x0
    huix0=uicontrol(hfig,"style","text");
    huix0.Position = [10 100 150 20];
    huix0.String = "x0="+sci2exp(x0);
    huix0.BackgroundColor=[1 1 1];
    //
    // A button to set x0
    huisetx0=uicontrol(hfig,"style","pushbutton");
    huisetx0.Position = [10 80 150 20];
    huisetx0.String = "Set x0";
    huisetx0.BackgroundColor=[0.9 0.9 0.9];
    huisetx0.Callback = "demo_rosensetx0";
    //
    // A button to Start the optimisation
    huirun=uicontrol(hfig,"style","pushbutton");
    huirun.Position = [10 60 150 20];
    huirun.String = "Run";
    huirun.BackgroundColor=[0.9 0.9 0.9];
    huirun.Callback = "demo_rosenrun";
    //
    // A button to Clear the plot
    huirun=uicontrol(hfig,"style","pushbutton");
    huirun.Position = [10 40 150 20];
    huirun.String = "Clear";
    huirun.BackgroundColor=[0.9 0.9 0.9];
    huirun.Callback = "demo_rosenclear";

    drawnow()

    global rosenshowgui;
    rosenshowgui = tlist(["ROSSHOGUI" "hplot" "hfig" "hmain" "huix0" "huisetx0" "huirun", "x0" "ninit"])
    rosenshowgui.hplot = hplot;
    rosenshowgui.hfig = hfig;
    rosenshowgui.hmain = hmain;
    rosenshowgui.huix0 = huix0;
    rosenshowgui.huisetx0 = huisetx0;
    rosenshowgui.x0 = x0;
    rosenshowgui.ninit = size(rosenshowgui.hplot.children.children,"*");

endfunction

function demo_rosensetx0()
    //
    // The user asks to set x0

    global rosenshowgui;
    huix0 = rosenshowgui.huix0;
    hplot = rosenshowgui.hplot;
    
    //
    // Display a message in the status bar
    scf(rosenshowgui.hplot)
    localstr = msprintf("Click to set x0")
    xinfo(gettext(localstr))
    //
    // Let the user click
    [ibutton,xcoord,yxcoord]=xclick()
    x0 = [xcoord,yxcoord]
    //
    // Clear the window
    demo_rosenclear ()
    //
    // Update data
    huix0.String = msprintf("x0=[%.2f,%.2f]",x0(1),x0(2));
    rosenshowgui.x0 = x0;
    xinfo("")
    delete(rosenshowgui.hplot.children.children(1))
    plot(x0(1) , x0(2) , "b.")

endfunction
function demo_rosenrun()
    //
    // The user asks to run the optimization

    global rosenshowgui;
    x0 = rosenshowgui.x0;

    //
    // 3. Use optim to find the minimum, starting from current x0
    scf(rosenshowgui.hplot)
    xopt = scidemo_optimrosenbrock ( x0 , %t );
    plot(xopt(1) , xopt(2) , "r.")
endfunction
function demo_rosenclear ()
    //
    // The user asks to clear the plot
    global rosenshowgui;
    hplot = rosenshowgui.hplot;
    ninit = rosenshowgui.ninit;

    n=size(rosenshowgui.hplot.children.children,"*")
    todelete = (1:n-ninit)
    if ( todelete <> [] ) then
      delete(rosenshowgui.hplot.children.children(todelete))
    end
endfunction


demo_rosenbrockshow ();
clear demo_rosenbrockshow;

