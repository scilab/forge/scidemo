// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function scidemo_matrixeigshow ( A )
    // Compute the eigenvalues of a square 2-by-2 matrix.
    //
    // Calling Sequence
    //   scidemo_matrixeigshow (A)
    //
    // Parameters
    //  A : a 2-by-2 real matrix of doubles
    //
    // Description
    //    This function makes use of the spec function to compute the 
    //    eigenvalues and eigenvectors of a square 2-by-2 matrix.
    //    
    //    This function first plot the unit cercle, using the equations 
    //
    //   <latex>
    //   \begin{eqnarray}
    //    && x_1 = r\sin(\theta), \quad \theta\in[0,2\pi], \\
    //    && x_2 = r\sin(\theta)
    //   \end{eqnarray}
    //   </latex>
    //
    //   Then, we plot the points b from the equation
    //
    //   <latex>
    //   \begin{eqnarray}
    //    b = Ax.
    //   \end{eqnarray}
    //   </latex>
    //
    //   The current function is a demonstration of the following functions.
    //   <itemizedlist>
    //   <listitem>Uses the * operator, which operates on matrices.</listitem>
    //   <listitem>Uses the spec function to compute the eigenvalues and eigenvectors.</listitem>
    //   </itemizedlist>
    //   
    //   The eigenvalues lambda and eigenvectors x are satisfying the equation:
    //
    //   <latex>
    //   \begin{eqnarray}
    //    Ax = \lambda x
    //   \end{eqnarray}
    //   </latex>
    //
    //   The spec function has the calling sequence 
    //
    //    <programlisting> 
    //    [R,D]=spec(A)
    //    </programlisting>
    //
    //    which decomposes the matrix A as 
    //
    //   <latex>
    //   \begin{eqnarray}
    //    A = R D R^{-1}
    //   \end{eqnarray}
    //   </latex>
    //
    //   where D is the diagonal matrix of eigenvalues and R is the 
    //   column-by-column matrix of right eigenvectors.
    //
    //   The two rays in the plot are corresponding to the two eigenvectors.
    //   
    //   It may happen that the two eigenvectors are not the major and
    //   minor axes of the ellipse. 
    //   If the matrix is symmetric, the two eigenvectors are the major 
    //   and minor axes. 
    //
    // Examples
    //   A = [1 2;3 4];
    //   [R,D]=spec(A)
    //   h = scf();
    //   scidemo_matrixeigshow (A);
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    //
    // Bibliography
    //   "Numerical Computing with Matlab", Cleve Moler, 2004, Chapter 10. Eigenvalues and Singular Values
    
    [lhs, rhs] = argn()
    apifun_checkrhs ( "scidemo_matrixeigshow" , rhs , 1 )
    apifun_checklhs ( "scidemo_matrixeigshow" , lhs , 0:1 )
    //
    // Check Type
    apifun_checktype ( "scidemo_matrixeigshow" , A , "A" , 1 , "constant" )
    //
    // Check Size
    apifun_checksquare ( "scidemo_matrixeigshow" , A , "A" , 1 )
    //
    // Check Content
    // Nothing to check

    drawlater()
    r = 1;
    m = 1000;
    t = linspace(0,2*%pi,m);
    //
    // Plot the unit circle
    x(1,:)= r*cos(t);
    x(2,:)= r*sin(t);
    plot(x(1,:),x(2,:),"g-")
    //
    // Plot the image ellipse
    b = A*x;
    plot(b(1,:),b(2,:),"r-")
    //
    // Compute the eigenvalues
    [R,D]=spec(A);
    //
    // Plot the eigenvectors
    for k = 1:2
      plot([0 R(1,k)],[0 R(2,k)],"bo-")
    end
    //
    // Make data_bounds the same for x and y, 
    // so that the circle appears as a circle.
    h = gcf()
    m = max(abs(h.children.data_bounds))
    h.children.data_bounds = [-m -m;m m]
    //
    // Make the figure size roughly square
    bs = 500
    h.figure_size = [bs 1.2*bs]
    //
    // Display the eigenvalues in the title
    // Remove % (in the potential a+%i*b) to prevent from
    // bad interaction with LaTeX
    if ( isreal(D(1,1)) ) then
      d1 = msprintf("%.3f",D(1,1))
    else
      d1 = msprintf("%.3f + i*%.3f",real(D(1,1)),imag(D(1,1)))
    end
    if ( isreal(D(2,2)) ) then
      d2 = msprintf("%.3f",D(2,2))
    else
      d2 = msprintf("%.3f + i*%.3f",real(D(2,2)),imag(D(2,2)))
    end
    thetitle = "Eigenvalues : l1=" + d1 + ", l2=" + d2
    xtitle(thetitle,"x(1)","x(2)")
    drawnow()
endfunction

