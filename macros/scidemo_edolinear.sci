// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = scidemo_edolinear ( varargin)
    // Solves a linear Differential Equation.
    //
    // Calling Sequence
    //   y = scidemo_edolinear ( y0 , t0 , t , A )
    //   y = scidemo_edolinear ( y0 , t0 , t , A , rtol )
    //   y = scidemo_edolinear ( y0 , t0 , t , A , rtol , atol )
    //   y = scidemo_edolinear ( y0 , t0 , t , A , rtol , atol , solname )
    //
    // Parameters
    //  y0 : a n-by-1 matrix of doubles, the initial state
    //  t0 : a 1-by-1 matrix of doubles, the initial time
    //  t : a m-by-1 matrix of doubles, the times where to compute the solution
    //  A : a n-by-n real matrix of doubles
    //  rtol : a 1-by-1 matrix of doubles, the relative tolerance (default rtol=1.d-5)
    //  atol : a 1-by-1 matrix of doubles, the absolute tolerance (default atol=1.d-7) 
    //  solname : a 1-by-1 matrix of strings, the name of the solver (default solname = "rk"). Solvers available are "adams" "stiff" "rk" "rkf" "fix".
    //  y : a m-by-1 matrix of doubles, the solution of the differential equation
    //
    // Description
    //   Computes the solution of a linear Differential Equation with the 
    //   ode function.
    //
    //   The current function is a demonstration of the following functions.
    //   <itemizedlist>
    //   <listitem>Uses the ode function to compute an approximate solution.</listitem>
    //   <listitem>Uses the expm function to compute the exact solution.</listitem>
    //   </itemizedlist>
    //
    //   We consider the following first order system of linear differential equations:
    //
    //   <latex>
    //   \begin{eqnarray}
    //   \frac{d}{dt} y(t) = Ay(t)
    //   \end{eqnarray}
    //   </latex>
    //
    //   with the initial state:
    //
    //   <latex>
    //   \begin{eqnarray}
    //   y(0) = y_0
    //   \end{eqnarray}
    //   </latex>
    //
    //   The solution of this equation is 
    //
    //   <latex>
    //   \begin{eqnarray}
    //   y(t) =  e^{At} y_0
    //   \end{eqnarray}
    //   </latex>
    //
    // Examples
    //   // To see the source code of this function:
    //   edit scidemo_edolinear
    //
    //   // Compute and print the approximate solution.
    //   scf();
    //   // The number of time samples.
    //   m = 100;
    //   // The initial state.
    //   y0 = [1;10];
    //   // The linear operator.
    //   A = [  1 1;  0 1.1];
    //   // Initial and final times.
    //   t0 = 0;
    //   tf = 5;
    //   t = linspace(t0,tf,100);
    //   // Compute the solution
    //   y = scidemo_edolinear ( y0 , t0 , t , A );
    //   subplot(1,2,1)
    //   // Plot y(1)
    //   plot(t,y(1,:),"bo")
    //   xtitle("","t (s)","y(1)")
    //   // Plot y(2)
    //   subplot(1,2,2)
    //   plot(t,y(2,:),"bo")
    //   xtitle("","t (s)","y(2)")
    //
    //   // Update the tolerances
    //   y = scidemo_edolinear ( y0 , t0 , t , A , 1.e-10 , 1.e-10 );
    //
    //   // Change solver
    //   y = scidemo_edolinear ( y0 , t0 , t , A , 1.e-5 , 1.e-7 , "fix" );
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    //
    // Bibliography
    //   http://en.wikipedia.org/wiki/Matrix_exponential
    //   "Syst�mes dynamiques", Claude Gomez, 2007, http://www.scilab.org/team/claude.gomez/papers/sistdynpdf.pdf
    
    [lhs, rhs] = argn()
    apifun_checkrhs ( "scidemo_edolinear" , rhs , 4:7 )
    apifun_checklhs ( "scidemo_edolinear" , lhs , 0:1 )
    //
    // Get optional input arguments
    y0 = varargin(1)
    t0 = varargin(2)
    t = varargin(3)
    A = varargin(4)
    if ( rhs <= 4 ) then
      rtol = 1.d-5
    else
      rtol = varargin(5)
    end
    if ( rhs <= 5 ) then
      atol = 1.d-7
    else
      atol = varargin(6)
    end
    if ( rhs <= 6 ) then
      solname = "rk"
    else
      solname = varargin(7)
    end
    //
    // Check Type
    apifun_checktype ( "scidemo_edolinear" , y0 , "y0" , 1 , "constant" )
    apifun_checktype ( "scidemo_edolinear" , t0 , "t0" , 2 , "constant" )
    apifun_checktype ( "scidemo_edolinear" , t , "t" , 3 , "constant" )
    apifun_checktype ( "scidemo_edolinear" , A , "A" , 4 , "constant" )
    apifun_checktype ( "scidemo_plotedolinear" , rtol , "rtol" , 5 , "constant" )
    apifun_checktype ( "scidemo_plotedolinear" , atol , "atol" , 6 , "constant" )
    apifun_checktype ( "scidemo_plotedolinear" , solname , "solname" , 7 , "string" )
    //
    // Check Size
    nv = size(y0,"*")
    apifun_checkvector ( "scidemo_edolinear" , y0 , "y0" , 1 , nv )
    apifun_checkscalar ( "scidemo_edolinear" , t0 , "t0" , 2 )
    apifun_checkvector ( "scidemo_edolinear" , t , "t" , 3 , size(t,"*") )
    apifun_checkdims ( "scidemo_edolinear" , A , "A" , 4 , [nv nv] )
    apifun_checkscalar ( "scidemo_plotedolinear" , rtol , "rtol" , 5 )
    apifun_checkscalar ( "scidemo_plotedolinear" , atol , "atol" , 6 )
    apifun_checkscalar ( "scidemo_plotedolinear" , solname , "solname" , 7 )
    //
    // Check Content
    apifun_checkrange ( "scidemo_normpdf" , rtol , "rtol" , 5 , number_properties("tiniest") , %inf )
    apifun_checkrange ( "scidemo_normpdf" , atol , "atol" , 6 , number_properties("tiniest") , %inf )
    apifun_checkoption ( "scidemo_plotedolinear" , solname , "solname" , 7 , ["adams" "stiff" "rk" "rkf" "fix"] )

    function y = scidemo_simulator ( t , x , A )
      y = A * x
    endfunction

    y = ode(solname,y0,t0,t,rtol,atol,list(scidemo_simulator,A));
endfunction

