// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function xopt = scidemo_optimrosenbrock ( x0 , doplotpoints )
    // Compute the solution of the Rosenbrock problem
    //
    // Calling Sequence
    //   xopt = scidemo_optimrosenbrock ( x0 , doplotpoints )
    //
    // Parameters
    //  x0 : a n-by-1 matrix of doubles, the initial guess
    //  doplotpoints : a 1-by-1 matrix of booleans, set to %t to plot the points during optimization
    //  xopt : a n-by-1 matrix of doubles, the solution
    //
    // Description
    //   Computes the solution of the Rosenbrock optimization test case.
    //   This test case has been used by H.H. Rosenbrock in 1964 and 
    //   has been used extensively in the optimization bibliography.
    //
    //   The current function is a demonstration of the following functions.
    //   <itemizedlist>
    //   <listitem>Uses the optim function to find the minimum.</listitem>
    //   <listitem>Uses the plot function to plot the intermediate points.</listitem>
    //   </itemizedlist>
    //
    //   We consider the following unconstrained optimization problem:
    //
    //   <latex>
    //   \begin{eqnarray}
    //   \min_{x\in\mathbb{R}^2} f(x) = 100(x_2-x_1^2)^2 + (1-x_1)^2
    //   \end{eqnarray}
    //   </latex>
    //
    //   with the initial guess:
    //
    //   <latex>
    //   \begin{eqnarray}
    //   x_0 = (-1.2,1)^T
    //   \end{eqnarray}
    //   </latex>
    //
    //   The global minimum of the objective function is reached 
    //   at 
    //
    //   <latex>
    //   \begin{eqnarray}
    //   x^\star = (1,1)^T
    //   \end{eqnarray}
    //   </latex>
    //
    // Examples
    //   // To see the source code of this function:
    //   edit scidemo_optimrosenbrock
    //   //
    //   // 1. Define rosenbrock for contouring
    //   function f = rosenbrockC ( x1 , x2 )
    //     x = [x1 x2];
    //     f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
    //   endfunction
    //   x0 = [-1.2 1.0];
    //   xopt = [1.0 1.0];
    //   //
    //   // 2. Draw the contour of Rosenbrock's function
    //   xdata = linspace(-2,2,100);
    //   ydata = linspace(-2,2,100);
    //   mprintf("Draw contours...\n");
    //   scf();
    //   contour ( xdata , ydata , rosenbrockC , [1 10 100 500 1000])
    //   plot(x0(1) , x0(2) , "b.")
    //   plot(xopt(1) , xopt(2) , "r*")
    //   xtitle("The Rosenbrock Test Case","x1","x2")
    //
    //   // 3. Use optim to find the minimum
    //   xopt = scidemo_optimrosenbrock ( x0 , %t );
    //   plot(xopt(1) , xopt(2) , "g.")
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    //
    // Bibliography
    //   "Unconstrained Optimality Conditions with Scilab", Michael Baudin, 2010, http://forge.scilab.org/index.php/p/docuncoptimcond
    //   "Introduction to Optimization with Scilab", Michael Baudin, 2010, http://forge.scilab.org/index.php/p/docintroscioptim/
    //   "An automatic method for finding the greatest or least value of a function", H. H. Rosenbrock.  The Computer Journal, 3(3):175{184, March 1960.

    [lhs, rhs] = argn()
    apifun_checkrhs ( "scidemo_optimrosenbrock" , rhs , 2 )
    apifun_checklhs ( "scidemo_optimrosenbrock" , lhs , 0:1 )
    //
    // Check Type
    apifun_checktype ( "scidemo_optimrosenbrock" , x0 , "x0" , 1 , "constant" )
    apifun_checktype ( "scidemo_optimrosenbrock" , doplotpoints , "doplotpoints" , 2 , "boolean" )
    //
    // Check Size
    apifun_checkvector ( "scidemo_optimrosenbrock" , x0 , "x0" , 1 , size(x0,"*") )
    apifun_checkscalar ( "scidemo_optimrosenbrock" , doplotpoints , "doplotpoints" , 2 )
    //
    // Check Content
    // Nothing to check

    //
    // Define Rosenbrock for optimization
    function [ f , g , ind ] = rosenbrock ( x , ind , doplotpoints )
        if ((ind == 1) | (ind == 4)) then
            f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
        end
        if ((ind == 1) | (ind == 4)) then
            g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
            g(2) = 200. * ( x(2) - x(1)**2 )
        end
        if (ind == 1 & doplotpoints ) then
            plot ( x(1) , x(2) , "g." )
        end
    endfunction
    //
    // Plot the optimization process, during optimization
    [ fopt , xopt ] = optim ( list(rosenbrock,doplotpoints) , x0 , imp = -1)
endfunction

