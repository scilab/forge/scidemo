// Copyright (C) 2001 - Bruno PINCON
// Copyright (C) 2005 - INRIA - Pierre MARECHAL 
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function scidemo_plotcmplxfun (R,e,TypeDomain,TypeCut,n,StrFunc,theta,alpha,DomReal)
    // Draw a complex function on a square or a disk.
    //
    // Calling Sequence
    //   scidemo_plotcmplxfun (R,e,TypeDomain,TypeCut,n,StrFunc,theta,alpha,DomReal)
    //
    // Parameters
    //  R : a 1-by-1 matrix of doubles, length of half a side of the square or radius of the disk
    //  e : a 1-by-1 matrix of doubles, thin layer to avoid the branch(es) cut(s)
    //  TypeDomain : a 1-by-1 matrix of strings, "Square" or "Disk"
    //  TypeCut : a 1-by-1 matrix of strings, "Ox" or "Oy"
    //  n : a scalar (for Square) or a 2-vector = [ntheta, nr] (for Disk) for discretization
    //  StrFunc : a 1-by-1 matrix of strings, the string which names the complex function (this is because primitive don't pass as function argument)
    //  theta : a 1-by-1 matrix of doubles, usual parameters for plot3d
    //  alpha : a 1-by-1 matrix of doubles, usual parameters for plot3d
    //  DomReal : a 1-by-2 matrix of doubles, interval for which the real restriction is drawn
    //
    // Description
    //    A function to draw on a square or a disk a complex function
    //    with branch(es) cut(s) on Ox or Oy.
    //
    // Examples
    //   scf();
    //   theta = -110;
    //   alpha = 75;
    //   scidemo_plotcmplxfun (2,%eps,"Square","Ox",41,"acos",theta,alpha,[-1,1]);
    //
    // Authors
    // Copyright (C) 2001 - Bruno PINCON
    // Copyright (C) 2005 - INRIA - Pierre MARECHAL 
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    //
    // Bibliography
    // "Cleve's Corner: Trigonometry Is a Complex Subject", Cleve Moler, 1998, http://www.mathworks.com/company/newsletters/news_notes/clevescorner/sum98cleve.html
    // "Classroom Tips and Techniques: Branches and Branch Cuts for the Inverse Trig and Hyperbolic Functions", Robert J. Lopez, 2008, http://www.maplesoft.com/view.aspx?SF=6932/Branches_and_Branch_Cuts.pdf

    [lhs, rhs] = argn()
    apifun_checkrhs ( "scidemo_plotcmplxfun" , rhs , 9 )
    apifun_checklhs ( "scidemo_plotcmplxfun" , lhs , 0:1 )
    //
    // Check Type
    apifun_checktype ( "scidemo_plotcmplxfun" , R , "R" , 1 , "constant" )
    apifun_checktype ( "scidemo_plotcmplxfun" , e , "e" , 2 , "constant" )
    apifun_checktype ( "scidemo_plotcmplxfun" , TypeDomain , "TypeDomain" , 3 , "string" )
    apifun_checktype ( "scidemo_plotcmplxfun" , TypeCut , "TypeCut" , 4 , "string" )
    apifun_checktype ( "scidemo_plotcmplxfun" , n , "n" , 5 , "constant" )
    apifun_checktype ( "scidemo_plotcmplxfun" , StrFunc , "StrFunc" , 6 , "string" )
    apifun_checktype ( "scidemo_plotcmplxfun" , theta , "theta" , 7 , "constant" )
    apifun_checktype ( "scidemo_plotcmplxfun" , alpha , "alpha" , 8 , "constant" )
    apifun_checktype ( "scidemo_plotcmplxfun" , DomReal , "DomReal" , 9 , "constant" )
    //
    // Check Size
    apifun_checkscalar ( "scidemo_plotcmplxfun" , R , "R" , 1 )
    apifun_checkscalar ( "scidemo_plotcmplxfun" , e , "e" , 2 )
    apifun_checkscalar ( "scidemo_plotcmplxfun" , TypeDomain , "TypeDomain" , 3 )
    apifun_checkscalar ( "scidemo_plotcmplxfun" , TypeCut , "TypeCut" , 4 )
    apifun_checkscalar ( "scidemo_plotcmplxfun" , StrFunc , "StrFunc" , 6 )
    apifun_checkscalar ( "scidemo_plotcmplxfun" , theta , "theta" , 7 )
    apifun_checkscalar ( "scidemo_plotcmplxfun" , alpha , "alpha" , 8 )
    apifun_checkvector ( "scidemo_plotcmplxfun" , DomReal , "DomReal" , 9 , 2 )
    //
    // Check Content
    apifun_checkrange ( "scidemo_plotcmplxfun" , R , "R" , 1 , number_properties("tiniest") , %inf )
    apifun_checkrange ( "scidemo_plotcmplxfun" , e , "nrandx" , 2 , 0 , %inf )
    apifun_checkoption ( "scidemo_plotcmplxfun" , TypeDomain , "TypeDomain" , 3 , ["Square" "Disk"] )
    apifun_checkoption ( "scidemo_plotcmplxfun" , TypeCut , "TypeCut" , 4 , ["Ox" "Oy"] )
    apifun_checkrange ( "scidemo_plotcmplxfun" , n , "n" , 5 , 1 , %inf )
    // theta : whatever
    // alpha : whatever
    apifun_checkgreq ( "scidemo_plotcmplxfun" , DomReal(2) , "DomReal(2)" , 9 , DomReal(1) )
    //
    // Special check for n
    if ( TypeDomain == "Square" ) then
      apifun_checkscalar ( "scidemo_plotcmplxfun" , n , "n" , 5 )
    else
      apifun_checkvector ( "scidemo_plotcmplxfun" , n , "n" , 5 , 2 )
    end
    //  
    // computes the facets
    [xr,yr,zr,xi,yi,zi] = scidemo_cmplxfacets (R,e,TypeDomain,TypeCut,n,StrFunc);

    // draw
    // ============================================
    
    // Title
    // ============================================

    Rs = string(R);

    if TypeDomain == "Square" then
        end_title = " Function on [-"+Rs+","+Rs+"]x[-"+Rs+","+Rs+"]"
    else
        end_title = " Function on D(0,R="+Rs+")"
    end

    if StrFunc == "f" then
        the_title = "Your Custom (named f) Complex" + end_title;
    else
        the_title = "The Complex " + StrFunc + end_title;
    end

    xtitle(the_title);


    // plot Re(z) + the real restriction
    // ============================================

    subplot(1,2,1)
    plot3d(xr,yr,zr);

    // real function in yellow
    // ============================================

    
    if DomReal(2) > DomReal(1) then
      thistitle = "Re("+StrFunc+"(z)) (In yellow : the real "+StrFunc+" function)"
    else
      thistitle = "Re("+StrFunc+"(z))"
    end
    xtitle(thistitle);

    if DomReal(2) > DomReal(1) then
        xx = linspace(DomReal(1),DomReal(2),40)';
        yy = zeros(xx);
        zz = evstr(StrFunc+"(xx)");
        param3d1(xx,yy,list(zz,32),theta,alpha,flag=[0,0]);
        yellow_line = get('hdl');
        yellow_line.thickness = 3;
    end

    // plot Im(z)
    // ============================================

    subplot(1,2,2)
    plot3d(xi,yi,zi);
    thistitle = "Im("+StrFunc+"(z))"
    xtitle(thistitle);
    
    //
    // Update the color map
    h=gcf()
    for i = 1 : size(h.children,"*")
      h.children(i).rotation_angles = [alpha theta];
    end
endfunction

