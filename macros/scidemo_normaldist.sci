// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function scidemo_normaldist ( mu , sigma , nrandx , nhclass , xmin , xmax )
    // Plots the exact Normal function and random generated points.
    //
    // Calling Sequence
    //   scidemo_normaldist ( mu , sigma , nrandx , nhclass )
    //
    // Parameters
    //  mu : a 1-by-1 matrix of doubles, the mean
    //  sigma : a 1-by-1 matrix of doubles, the variance
    //  nrandx : a 1-by-1 matrix of doubles, the number of generated random numbers
    //  nhclass : a 1-by-1 matrix of doubles, the number of classes in the histogram
    //  xmin : a 1-by-1 matrix of doubles, the minimum x
    //  xmax : a 1-by-1 matrix of doubles, the maximum x
    //
    // Description
    //   Computes the exact distribution function of the Normal (Laplace-Gauss) 
    //   distribution function in the range [xmin,xmax] and draw it.
    //   Then generate nrandx points and draw the histogram with 
    //   nhclass classes.
    //   Then plots the intervals [mu-n*sigma,mu+n*sigma] for n=1,2,3.
    //   In the legend, display the corresponding integrals approximated from the 
    //   intg function.
    //
    //   The current function is a demonstration of the following functions.
    //   <itemizedlist>
    //   <listitem>Uses the grand function to generate the random numbers.</listitem>
    //   <listitem>Uses the histo function to draw the histogram.</listitem>
    //   <listitem>Uses the intg function to compute the approximate integrals.</listitem>
    //   <listitem>Uses the erf function to compute the exact integrals.</listitem>
    //   </itemizedlist>
    //
    //   The Normal distribution function definition is:
    //
    //<latex>
    //\begin{eqnarray}
    //f(x,\mu,\sigma) = \frac{1}{\sigma\sqrt{2\pi}} \exp\left(\frac{-(x-\mu)^2}{2\sigma^2}\right)
    //\end{eqnarray}
    //</latex>
    //
    //   This implies that, if X is a random variable from the Normal 
    //   distribution, the probability that X is inside the interval [a,b] is 
    //
    //<latex>
    //\begin{eqnarray}
    // P(a \leq X \leq b) = \int_a^b f(x,\mu,\sigma) dx
    //\end{eqnarray}
    //</latex>
    //
    // The previous equality can be associated with the erf function, which 
    // satisfies the equality
    //
    //<latex>
    //\begin{eqnarray}
    // P(\mu-n\sigma \leq X \leq \mu+n\sigma) = \textrm{erf}(n/\sqrt{2})
    //\end{eqnarray}
    //</latex>
    //
    // where n=1,2,3,....
    //
    // Examples
    //   // Compare generated Normal random numbers with exact PDF.
    //   mu = 10;
    //   sigma = 5;
    //   xmin = mu-6*sigma;
    //   xmax = mu+6*sigma;
    //   nrandx = 10000;
    //   nhclass = 100;
    //   scf();
    //   scidemo_normaldist ( mu , sigma , nrandx , nhclass , xmin , xmax );
    //   // Uses erf and compare with approximate computations
    //   erf([1 2 3]/sqrt(2))
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    //
    // Bibliography
    //   distfun/normpdf, 2009-2010 - DIGITEO - Michael Baudin, http://forge.scilab.org/index.php/p/distfun/source/tree/HEAD/macros/normpdf.sci
    
    [lhs, rhs] = argn()
    apifun_checkrhs ( "scidemo_normaldist" , rhs , 6 )
    apifun_checklhs ( "scidemo_normaldist" , lhs , 0:1 )
    //
    // Check Type
    apifun_checktype ( "scidemo_normaldist" , mu , "mu" , 1 , "constant" )
    apifun_checktype ( "scidemo_normaldist" , sigma , "sigma" , 2 , "constant" )
    apifun_checktype ( "scidemo_normaldist" , nrandx , "nrandx" , 3 , "constant" )
    apifun_checktype ( "scidemo_normaldist" , nhclass , "nhclass" , 4 , "constant" )
    apifun_checktype ( "scidemo_normaldist" , xmin , "xmin" , 5 , "constant" )
    apifun_checktype ( "scidemo_normaldist" , xmax , "xmax" , 6 , "constant" )
    //
    // Check Size
    apifun_checkscalar ( "scidemo_normaldist" , mu , "mu" , 1 )
    apifun_checkscalar ( "scidemo_normaldist" , sigma , "sigma" , 2 )
    apifun_checkscalar ( "scidemo_normaldist" , nrandx , "nrandx" , 3 )
    apifun_checkscalar ( "scidemo_normaldist" , nhclass , "nhclass" , 4 )
    apifun_checkscalar ( "scidemo_normaldist" , xmin , "xmin" , 5 )
    apifun_checkscalar ( "scidemo_normaldist" , xmax , "xmax" , 6 )
    //
    // Check Content
    apifun_checkrange ( "scidemo_normaldist" , sigma , "sigma" , 2 , number_properties("tiniest") , %inf )
    apifun_checkrange ( "scidemo_normaldist" , nrandx , "nrandx" , 3 , 1 , %inf )
    apifun_checkrange ( "scidemo_normaldist" , nhclass , "nhclass" , 4 , 1 , %inf )
    apifun_checkgreq ( "scidemo_normaldist" , xmax , "xmax" , 6 , xmin )
    //
    // Draw PDF
    nx = 1000
    x = linspace(xmin,xmax,nx);
    p = scidemo_normpdf ( x , mu*ones(1,nx) , sigma*ones(1,nx) );
    plot(x,p)
    //
    // Add histogram
    r = grand(nrandx,1,"nor",mu,sigma);
    histplot(nhclass,r)
    thetitle = msprintf("Normal Probability Distribution Function - mu=%.3f, sigma=%.3f",mu,sigma)
    //
    // Add 1 sigma
    pmax = max(p)
    p1 = -0.05*pmax;
    x1min = mu-sigma;
    x1max = mu+sigma;
    plot([x1min x1max],[p1 p1],"rx-")
    //
    // Add 2 sigma
    p2 = -0.1*pmax;
    x2min = mu-2*sigma;
    x2max = mu+2*sigma;
    plot([x2min x2max],[p2 p2],"gx-")
    //
    // Add 3 sigma
    p3 = -0.15*pmax;
    x3min = mu-3*sigma;
    x3max = mu+3*sigma;
    plot([x3min x3max],[p3 p3],"bx-")
    //
    // Compute the 3 legends for the 3 values of sigma
    I1 = intg(mu-sigma,mu+sigma,list(scidemo_normpdf,mu,sigma))
    I2 = intg(mu-2*sigma,mu+2*sigma,list(scidemo_normpdf,mu,sigma))
    I3 = intg(mu-3*sigma,mu+3*sigma,list(scidemo_normpdf,mu,sigma))
    legendsigma1 = msprintf("1 sigma=%.1f%%",I1*100)
    legendsigma2 = msprintf("2 sigma=%.1f%%",I2*100)
    legendsigma3 = msprintf("3 sigma=%.1f%%",I3*100)
    //
    // Add title, legend
    legend(["Exact" "grand" legendsigma1 legendsigma2 legendsigma3])
    xtitle(thetitle , "X" , "Probability" )
    //
    // Fixed the data bounds
    ymin = p3
    ymax = 1.05*max(p)
    h = gcf()
    h.children.data_bounds = [
      xmin  ymin
      xmax  ymax
    ];
endfunction


