// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function p = scidemo_normpdf ( x , mu , sigma )
  // Returns the Normal (Laplace-Gauss) probability distribution function.
  //
  // Calling Sequence
  //   p = normpdf ( x , mu , sigma )
  //
  // Parameters
  //   x : n-by-m matrix of doubles, the outcome
  //   mu : n-by-m matrix of doubles, the mean (default mu=0)
  //   sigma : n-by-m matrix of doubles, the standard deviation (default sigma=1)
  //   p : a n-by-m matrix of doubles, the probability
  //
  // Description
  //   Computes the probability distribution function of the Normal (Laplace-Gauss) function.
  //   
  //   Any scalar input argument is expanded to a matrix of doubles of the same size as the other input arguments.
  //
  //   The function definition is:
  //
  //<latex>
  //\begin{eqnarray}
  //f(x,\mu,\sigma) = \frac{1}{\sigma\sqrt{2\pi}} \exp\left(\frac{-(x-\mu)^2}{2\sigma^2}\right)
  //\end{eqnarray}
  //</latex>
  //
  // Examples
  //   computed = scidemo_normpdf ( [-1 1] , zeros(1,2) , ones(1,2) )
  //   expected = [ 0.241970724519143   0.241970724519143 ];
  //
  // // Plot the function
  // scf();
  // mu = [0 0 0 -2];
  // sigma2 = [0.2 1.0 5.0 0.5];
  // cols = [1 2 3 4];
  // nf = size(cols,"*");
  // lgd = [];
  // nx = 1000;
  // for k = 1 : nf
  //   x = linspace(-5,5,nx);
  //   y = scidemo_normpdf ( x , mu(k)*ones(1,nx) , sqrt(sigma2(k))*ones(1,nx) );
  //   plot(x,y)
  //   str = msprintf("mu=%s, sigma^2=%s",string(mu(k)),string(sigma2(k)));
  //   lgd($+1) = str;
  // end
  // h = gcf();
  // for k = 1 : nf
  //   h.children.children.children(nf - k + 1).foreground = cols(k);
  // end
  // legend(lgd);
  // xtitle("Normal Probability Distribution Function","X","Probability")
  //
  // Authors
  //   DIGITEO, Michael Baudin, 2010

  [lhs,rhs]=argn()
  apifun_checkrhs ( "scidemo_normpdf" , rhs , [1 3] )
  apifun_checklhs ( "scidemo_normpdf" , lhs , 0:1 )
  //
  // Check Type
  apifun_checktype ( "scidemo_normpdf" , x , "x" , 1 , "constant" )
  apifun_checktype ( "scidemo_normpdf" , mu , "mu" , 2 , "constant" )
  apifun_checktype ( "scidemo_normpdf" , sigma , "sigma" , 3 , "constant" )
  //
  // Check Size
  [mx,nx] = size(x)
  apifun_checkdims ( "scidemo_normpdf" , mu , "mu" , 2 , [mx,nx] )
  apifun_checkdims ( "scidemo_normpdf" , sigma , "sigma" , 3 , [mx,nx] )
  //
  // Check Content
  apifun_checkrange ( "scidemo_normpdf" , sigma , "sigma" , 2 , number_properties("tiniest") , %inf )
  //
  y = ( x - mu ) ./ sigma
  p = exp ( - 0.5 .* y .* y )  ./ ( sigma .* sqrt ( 2.0 * %pi ) )
endfunction

