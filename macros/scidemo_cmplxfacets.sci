// Copyright (C) 2001 - Bruno PINCON
// Copyright (C) 2005 - INRIA - Pierre MARECHAL 
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [xr,yr,zr,xi,yi,zi] = scidemo_cmplxfacets (R,e,TypeDomain,TypeCut,n,StrFunc)
    // Compute the facets to draw a complex function.
    //
    // Calling Sequence
    //   [xr,yr,zr,xi,yi,zi] = scidemo_cmplxfacets (R,e,TypeDomain,TypeCut,n,StrFunc)
    //
    // Parameters
    //  R : length of half a side of the square or radius of the disk
    //  e : thin layer to avoid the branch(es) cut(s)
    //  TypeDomain : a 1-by-1 matrix of strings, "Square" or "Disk"
    //  TypeCut : a 1-by-1 matrix of strings, "Ox" or "Oy"
    //  n : a scalar (for "Square") or a 2-vector = [ntheta, nr] (for "Disk") for discretization
    //  StrFunc : a 1-by-1 matrix of strings, the string which names the complex function (this is because primitive don't pass as function argument)
    //  xr,yr,zr,xi,yi,zi : a m-ny-n matrix of doubles, real and imaginary parts of X,Y and Z values
    //
    // Description
    //    A function to compute the facets for drawing a complex function
    //    on a square or a disk with branch(es) cut(s) on Ox or Oy
    //
    // Examples
    //   [xr,yr,zr,xi,yi,zi] = scidemo_cmplxfacets(2,%eps,"Square","Ox",41,"acos");
    //
    // Authors
    // Copyright (C) 2001 - Bruno PINCON
    // Copyright (C) 2005 - INRIA - Pierre MARECHAL 
    // Copyright (C) 2010 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "scidemo_cmplxfacets" , rhs , 6 )
    apifun_checklhs ( "scidemo_cmplxfacets" , lhs , 0:6 )
    //
    // Check Type
    apifun_checktype ( "scidemo_cmplxfacets" , R , "R" , 1 , "constant" )
    apifun_checktype ( "scidemo_cmplxfacets" , e , "e" , 2 , "constant" )
    apifun_checktype ( "scidemo_cmplxfacets" , TypeDomain , "TypeDomain" , 3 , "string" )
    apifun_checktype ( "scidemo_cmplxfacets" , TypeCut , "TypeCut" , 4 , "string" )
    apifun_checktype ( "scidemo_cmplxfacets" , n , "n" , 5 , "constant" )
    apifun_checktype ( "scidemo_cmplxfacets" , StrFunc , "StrFunc" , 6 , "string" )
    //
    // Check Size
    apifun_checkscalar ( "scidemo_cmplxfacets" , R , "R" , 1 )
    apifun_checkscalar ( "scidemo_cmplxfacets" , e , "e" , 2 )
    apifun_checkscalar ( "scidemo_cmplxfacets" , TypeDomain , "TypeDomain" , 3 )
    apifun_checkscalar ( "scidemo_cmplxfacets" , TypeCut , "TypeCut" , 4 )
    apifun_checkscalar ( "scidemo_cmplxfacets" , StrFunc , "StrFunc" , 6 )
    //
    // Check Content
    apifun_checkrange ( "scidemo_cmplxfacets" , R , "R" , 1 , number_properties("tiniest") , %inf )
    apifun_checkrange ( "scidemo_cmplxfacets" , e , "nrandx" , 2 , 0 , %inf )
    apifun_checkoption ( "scidemo_cmplxfacets" , TypeDomain , "TypeDomain" , 3 , ["Square" "Disk"] )
    apifun_checkoption ( "scidemo_cmplxfacets" , TypeCut , "TypeCut" , 4 , ["Ox" "Oy"] )
    apifun_checkrange ( "scidemo_cmplxfacets" , n , "n" , 5 , 1 , %inf )
    //
    // Special check for n
    if ( TypeDomain == "Square" ) then
      apifun_checkscalar ( "scidemo_cmplxfacets" , n , "n" , 5 )
    else
      apifun_checkvector ( "scidemo_cmplxfacets" , n , "n" , 5 , 2 )
    end
    //
    if TypeDomain == "Square" then
        if TypeCut == "Ox" then
            x1 = linspace(-R, R, floor(n));
            y1 = linspace( e, R, floor(n/2));
        else  // for TypeCut = "Oy" ...
            x1 = linspace( e, R, floor(n/2));
            y1 = linspace(-R, R, floor(n));
        end
        X1 = ones(y1')*x1 ; Y1 = y1'*ones(x1);
    else 
        // for TypeDomain = "Disk"
        r = linspace(0, R, floor(n(2)));
        if TypeCut == "Ox" then
            theta = linspace(0, %pi, floor(n(1)))';
            X1 = cos(theta)*r;
            Y1 = e + sin(theta)*r;
        else // for TypeCut = "Oy"
            theta = linspace(-%pi/2, %pi/2, floor(n(1)))';
            X1 = e + cos(theta)*r;
            Y1 = sin(theta)*r;
        end
    end
    X2 = -X1 ; 
    Y2 = -Y1;
    Z1 = evstr(StrFunc+"(X1 + %i*Y1)");
    Z2 = evstr(StrFunc+"(X2 + %i*Y2)");
    [xr1,yr1,zr1] = nf3d(X1,Y1,real(Z1));
    [xr2,yr2,zr2] = nf3d(X2,Y2,real(Z2));
    xr = [xr1 xr2];
    yr = [yr1 yr2];
    zr = [zr1 zr2];
    [xi1,yi1,zi1] = nf3d(X1,Y1,imag(Z1));
    [xi2,yi2,zi2] = nf3d(X2,Y2,imag(Z2));
    xi = [xi1 xi2];
    yi = [yi1 yi2];
    zi = [zi1 zi2];
endfunction

