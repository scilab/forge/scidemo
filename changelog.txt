changelog of the Scilab Demonstration Toolbox

scidemo (0.2)
    * Used apifun to check input arguments in all functions.
    * Added interactive demo for odelinear.
    * Added unit test and used apifun in scidemo_matrixeigshow.
    * Added unit test and used apifun in scidemo_optimrosenbrock.
    * Added unit test and used apifun in scidemo_edolinear.
    * Added unit test and used apifun in scidemo_cmplxfacets.
    * Added unit test and used apifun in scidemo_plotcmplxfun.
    * Added unit test and used apifun in scidemo_normpdf.
    * Added unit test and used apifun in scidemo_normaldist.
    * Added interactive demo for Normal Distribution Function.
    * Added interactive demo for Optimum of Rosenbrock.
    * Added demo function for Normal Distribution Function.
    * Added demo function for complex elementary functions.
    * Updated the Powerpoint.

scidemo (0.1)
    * Added demo function for eigenvalues of a matrix.
    * Added demo function for Optimum of Rosenbrock.
    * Added demo function for a linear EDO.


