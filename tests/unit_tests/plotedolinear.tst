// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

// Compute and print the approximate solution.
// The number of time samples.
m = 100;
// The initial state.
y0 = [1;10];
// The linear operator.
A = [  1 1;  0 1.1];
// Initial and final times.
t0 = 0;
tf = 5;
t = linspace(t0,tf,100);
// Make the plot
scf();
scidemo_plotedolinear ( y0 , t0 , t , A )

// Reduce the tolerances
scf();
scidemo_plotedolinear ( y0 , t0 , t , A , 1e-12 , 1e-12 )

// Use another solver
h = scf();
scidemo_plotedolinear ( y0 , t0 , t , A , 1e-5 , 1e-7 , "fix" )

