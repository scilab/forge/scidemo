// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then bugmes();quit;end
endfunction
// Compute and print the approximate solution.
// The number of time samples.
m = 100;
// The initial state.
y0 = [1;10];
// The linear operator.
A = [  1 1;  0 1.1];
// Initial and final times.
t0 = 0;
tf = 5;
t = linspace(t0,tf,100);
// Compute the solution
y = scidemo_edolinear ( y0 , t0 , t , A );
subplot(2,2,1)
// Plot y(1)
plot(t,y(1,:),"bo")
xtitle("","t (s)","y(1)")
// Plot y(2)
subplot(2,2,2)
plot(t,y(2,:),"bo")
xtitle("","t (s)","y(2)")
//
// Use other tolerances
y = scidemo_edolinear ( y0 , t0 , t , A , 1.e-10 , 1.e-10 );
//
// Use other solvers
for solname = ["adams" "stiff" "rk" "rkf" "fix"]
  y = scidemo_edolinear ( y0 , t0 , t , A , 1.e-5 , 1.e-7 , solname );
end
// Compute and print the exact solution.
// Uses the expm function to compute the matrix exponential.
yexact = zeros(2,m);
for k = 1 : m
tk = t(k);
yexact(:,k) = expm(A*tk)*y0;
end
subplot(2,2,1)
plot(t,yexact(1,:),"r-")
legend(["Approximate" "Exact"]);
subplot(2,2,2)
plot(t,yexact(2,:),"r-")
legend(["Approximate" "Exact"]);
// Print the relative error
relerr = abs(yexact-y)./abs(y);
subplot(2,2,3)
plot(t,relerr(1,:),"r-")
xtitle("","t (s)","Relative Error on y(1)")
subplot(2,2,4)
plot(t,relerr(2,:),"r-")
xtitle("","t (s)","Relative Error on y(2)")
