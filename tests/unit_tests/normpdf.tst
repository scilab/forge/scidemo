// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

computed = scidemo_normpdf ( [-1 1] , zeros(1,2) , ones(1,2) );
expected = [ 0.241970724519143   0.241970724519143 ];
assert_close ( computed , expected , 10 * %eps );

// Plot the function
scf();
mu = [0 0 0 -2];
sigma2 = [0.2 1.0 5.0 0.5];
cols = [1 2 3 4];
nf = size(cols,"*");
lgd = [];
nx = 1000;
for k = 1 : nf
x = linspace(-5,5,nx);
y = scidemo_normpdf ( x , mu(k)*ones(1,nx) , sqrt(sigma2(k))*ones(1,nx) );
plot(x,y)
str = msprintf("mu=%s, sigma^2=%s",string(mu(k)),string(sigma2(k)));
lgd($+1) = str;
end
h = gcf();
for k = 1 : nf
h.children.children.children(nf - k + 1).foreground = cols(k);
end
legend(lgd);
xtitle("Normal Probability Distribution Function","X","Probability");


