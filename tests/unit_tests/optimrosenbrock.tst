// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// 1. Define rosenbrock for contouring
function f = rosenbrockC ( x1 , x2 )
x = [x1 x2];
f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction
x0 = [-1.2 1.0];
xopt = [1.0 1.0];
//
// 2. Draw the contour of Rosenbrock's function
xdata = linspace(-2,2,100);
ydata = linspace(-2,2,100);
mprintf("Draw contours...\n");
scf();
contour ( xdata , ydata , rosenbrockC , [1 10 100 500 1000])
plot(x0(1) , x0(2) , "b.")
plot(xopt(1) , xopt(2) , "r*")
xtitle("The Rosenbrock Test Case","x1","x2")

// 3. Use optim to find the minimum
xopt = scidemo_optimrosenbrock ( x0 , %t );
plot(xopt(1) , xopt(2) , "g.")

