<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from scidemo_normaldist.sci using help_from_sci().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="scidemo_normaldist" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>scidemo_normaldist</refname><refpurpose>Plots the exact Normal function and random generated points.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   scidemo_normaldist ( mu , sigma , nrandx , nhclass )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>mu :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the mean</para></listitem></varlistentry>
   <varlistentry><term>sigma :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the variance</para></listitem></varlistentry>
   <varlistentry><term>nrandx :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the number of generated random numbers</para></listitem></varlistentry>
   <varlistentry><term>nhclass :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the number of classes in the histogram</para></listitem></varlistentry>
   <varlistentry><term>xmin :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the minimum x</para></listitem></varlistentry>
   <varlistentry><term>xmax :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the maximum x</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the exact distribution function of the Normal (Laplace-Gauss)
distribution function in the range [xmin,xmax] and draw it.
Then generate nrandx points and draw the histogram with
nhclass classes.
Then plots the intervals [mu-n*sigma,mu+n*sigma] for n=1,2,3.
In the legend, display the corresponding integrals approximated from the
intg function.
   </para>
   <para>
The current function is a demonstration of the following functions.
<itemizedlist>
<listitem>Uses the grand function to generate the random numbers.</listitem>
<listitem>Uses the histo function to draw the histogram.</listitem>
<listitem>Uses the intg function to compute the approximate integrals.</listitem>
<listitem>Uses the erf function to compute the exact integrals.</listitem>
</itemizedlist>
   </para>
   <para>
The Normal distribution function definition is:
   </para>
   <para>
<latex>
\begin{eqnarray}
f(x,\mu,\sigma) = \frac{1}{\sigma\sqrt{2\pi}} \exp\left(\frac{-(x-\mu)^2}{2\sigma^2}\right)
\end{eqnarray}
</latex>
   </para>
   <para>
This implies that, if X is a random variable from the Normal
distribution, the probability that X is inside the interval [a,b] is
   </para>
   <para>
<latex>
\begin{eqnarray}
P(a \leq X \leq b) = \int_a^b f(x,\mu,\sigma) dx
\end{eqnarray}
</latex>
   </para>
   <para>
The previous equality can be associated with the erf function, which
satisfies the equality
   </para>
   <para>
<latex>
\begin{eqnarray}
P(\mu-n\sigma \leq X \leq \mu+n\sigma) = \textrm{erf}(n/\sqrt{2})
\end{eqnarray}
</latex>
   </para>
   <para>
where n=1,2,3,....
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Compare generated Normal random numbers with exact PDF.
mu = 10;
sigma = 5;
xmin = mu-6*sigma;
xmax = mu+6*sigma;
nrandx = 10000;
nhclass = 100;
scf();
scidemo_normaldist ( mu , sigma , nrandx , nhclass , xmin , xmax );
// Uses erf and compare with approximate computations
erf([1 2 3]/sqrt(2))

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2010 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>distfun/normpdf, 2009-2010 - DIGITEO - Michael Baudin, http://forge.scilab.org/index.php/p/distfun/source/tree/HEAD/macros/normpdf.sci</para>
</refsection>
</refentry>
