<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from scidemo_edolinear.sci using help_from_sci().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="scidemo_edolinear" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>scidemo_edolinear</refname><refpurpose>Solves a linear Differential Equation.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   y = scidemo_edolinear ( y0 , t0 , t , A )
   y = scidemo_edolinear ( y0 , t0 , t , A , rtol )
   y = scidemo_edolinear ( y0 , t0 , t , A , rtol , atol )
   y = scidemo_edolinear ( y0 , t0 , t , A , rtol , atol , solname )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>y0 :</term>
      <listitem><para> a n-by-1 matrix of doubles, the initial state</para></listitem></varlistentry>
   <varlistentry><term>t0 :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the initial time</para></listitem></varlistentry>
   <varlistentry><term>t :</term>
      <listitem><para> a m-by-1 matrix of doubles, the times where to compute the solution</para></listitem></varlistentry>
   <varlistentry><term>A :</term>
      <listitem><para> a n-by-n real matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>rtol :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the relative tolerance (default rtol=1.d-5)</para></listitem></varlistentry>
   <varlistentry><term>atol :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the absolute tolerance (default atol=1.d-7)</para></listitem></varlistentry>
   <varlistentry><term>solname :</term>
      <listitem><para> a 1-by-1 matrix of strings, the name of the solver (default solname = "rk"). Solvers available are "adams" "stiff" "rk" "rkf" "fix".</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a m-by-1 matrix of doubles, the solution of the differential equation</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the solution of a linear Differential Equation with the
ode function.
   </para>
   <para>
The current function is a demonstration of the following functions.
<itemizedlist>
<listitem>Uses the ode function to compute an approximate solution.</listitem>
<listitem>Uses the expm function to compute the exact solution.</listitem>
</itemizedlist>
   </para>
   <para>
We consider the following first order system of linear differential equations:
   </para>
   <para>
<latex>
\begin{eqnarray}
\frac{d}{dt} y(t) = Ay(t)
\end{eqnarray}
</latex>
   </para>
   <para>
with the initial state:
   </para>
   <para>
<latex>
\begin{eqnarray}
y(0) = y_0
\end{eqnarray}
</latex>
   </para>
   <para>
The solution of this equation is
   </para>
   <para>
<latex>
\begin{eqnarray}
y(t) =  e^{At} y_0
\end{eqnarray}
</latex>
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// To see the source code of this function:
edit scidemo_edolinear

// Compute and print the approximate solution.
scf();
// The number of time samples.
m = 100;
// The initial state.
y0 = [1;10];
// The linear operator.
A = [  1 1;  0 1.1];
// Initial and final times.
t0 = 0;
tf = 5;
t = linspace(t0,tf,100);
// Compute the solution
y = scidemo_edolinear ( y0 , t0 , t , A );
subplot(1,2,1)
// Plot y(1)
plot(t,y(1,:),"bo")
xtitle("","t (s)","y(1)")
// Plot y(2)
subplot(1,2,2)
plot(t,y(2,:),"bo")
xtitle("","t (s)","y(2)")

// Update the tolerances
y = scidemo_edolinear ( y0 , t0 , t , A , 1.e-10 , 1.e-10 );

// Change solver
y = scidemo_edolinear ( y0 , t0 , t , A , 1.e-5 , 1.e-7 , "fix" );

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2010 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>http://en.wikipedia.org/wiki/Matrix_exponential</para>
   <para>"Systèmes dynamiques", Claude Gomez, 2007, http://www.scilab.org/team/claude.gomez/papers/sistdynpdf.pdf</para>
</refsection>
</refentry>
