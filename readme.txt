Scilab Demonstration toolbox

Purpose
-------

This is a project providing demonstrations illustrating numerical computing 
features in Scilab.
The purpose of this module is mainly educational, in the sense that 
it provides mathematical explanations of the mathematics behind the 
computations which are showed.
We use the LaTeX features of the Scilab v5 help management 
system to provide the equations to the user.

Dependencies
------------

This module uses the apifun module for argument checking.

List of demonstrations
-----------------------

 * Complex Elementary functions
 * Linear Algebra
 * Differential Equations
 * Optimization
 * Probabilities / Statistics

Forge 
-----

http://forge.scilab.org/index.php/p/scidemo/

ATOMS
-----

http://atoms.scilab.org/toolboxes/scidemo


TODO 
----
 * Report the interactive controls for the demo of complex functions.

Author
------

 * 2010 - DIGITEO - Michael Baudin
 * 2008 - INRIA
 * 2005 - INRIA - Pierre MARECHAL 
 * 2001 - Bruno Pincon

Licence
------

This toolbox is distributed under the CeCILL license :
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Acknowledgements
----------------

Michael Baudin thanks Allan Cornet and Didier Halgand for their input 
on this project.

